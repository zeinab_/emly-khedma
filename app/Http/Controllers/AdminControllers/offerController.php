<?php

namespace App\Http\Controllers\AdminControllers;
use App\Models\UserModels\Offer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class offerController extends Controller
{
    public function showOfferPage(){
    	$offer=Offer::get();
    	return view('admininterface.offerPage',['offer'=>$offer]);
    }
    public function Offer_add(Request $req)
    {
    	$req->validate([
    		'hour'=>'required|numeric',
    		'hours'=>'required|numeric',
    		'user'=>'required',
    		'service'=>'required'

    	]);
    	      Offer::create([
    	      	'price_of_hour'=>$req->hour,
    	      	'hours'=>$req->hours,
    	      	'user_id'=>$req->user,
    	      	'service_id'=>$req->service,
    	      ]);
    	        return response()->json();
    }
    public function offer_show(Request $rq)
    {
    	$offer_data=Offer::find($rq->id);
    	return response()->json($offer_data);

    }
    public function offer_update(Request $rt)
    {
    	$rt->validate([
    		'hour_e'=>'required|numeric',
    		'hours_e'=>'required|numeric',
    		'users_e'=>'required',
    		'service_e'=>'required'

    	]);
    	$offer_data=Offer::find($rt->id);
    	$offer_data->update
    	([
    		'price_of_hour'=>$rt->hour_e,
    		'hours'=>$rt->hours_e,
    		'user_id'=>$rt->users_e,
    		'service_id'=>$rt->service_e,
    	]);
    	  return response()->json();

    }
    public function delete_offer(Request $offer){
    	Offer::find($offer->id)->delete();

    }

}
