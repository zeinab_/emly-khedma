<?php

namespace App\Http\Controllers\AdminControllers;
use App\Models\AdminModels\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class AdminInfoController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }
    public function showLoginForm(){
        return view('admininterface.adminLogin');
    }
    public function Login(Request $req)
    {
        $this->validate($req, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (auth()->guard('admin')->attempt(['email' => $req->email, 'password' => $req->password]))
        {
            // $user = auth()->guard('admin')->user();
            // \Session::put('success','You are Login successfully!!');
            $session= DB::table('admins')->where('email',$req->email)->get();
            foreach ($session as $se) 
            {
                $info=['img'=>$se->img,
                'username'=>$se->username

                ];
             session()->put('id',$info);
              return redirect()->route('admin');
            
            }
        }
             else {
                        return redirect()->route('loginForm');

            // return back()->with('error','your username and password are wrong.');
        }




    }
}
