<?php

namespace App\Http\Controllers\AdminControllers;
use App\Models\UserModels\Service;
use App\Models\AdminModels\Department;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class servicesController extends Controller
{
    public function servicePage(){
    	$services=Service::get();
    	$depart  =Department::get();
    	$user    =User::get();

    	return view('adminInterface.servicePage',['services'=>$services,'depart'=>$depart,'user'=>$user]);


    }
    public function add_service(Request $req){
    	$req->validate
    	(['depart'=>'required',
    		'desc'=>'required',
    		'end'=>'required',
    		'start'=>'required',
    		'name'=>'required',
    		'place'=>'required',
    		'user'=>'required'
    	]);
    	Service::create
    	([
    		'name'=>$req->name,
    		'description'=>$req->desc,
    		'date_of_start'=>$req->start,
    		'date_of_end'=>$req->end,
    		'place'=>$req->place,
    		'department_id'=>$req->depart,
    		'user_id'=>$req->user,

    	]);

    }
    public function show_service(Request $request){
    	$data_s=Service::find($request->id);
        return response()->json($data_s);


    }
    public function update_service(Request $re){
     // return $re->all();
        $pattern="/[A-Za-z0-9]/";
        $patter="/[A-Za-z]/";

        if(preg_match($pattern, $re->name_e)||preg_match($pattern, $re->desc_e)||preg_match($patter, $re->place_e)){
            return"please write arabic";

        }
       
        else
        {
           $data_u=Service::find($re->edit_s);
          $data_u->update([
            'name'=>$re->name_e,
            'description'=>$re->desc_e,
            'place'=>$re->place_e,
            'start_of_date'=>$re->start_e,
            'end_of_date'=>$re->end_e,
            'department_id'=>$re->depart_e,
            'user_id'=>$re->user_e,

          ]);
          return "done";
        }

    }
    public function delete_service(Request $id){

     Service::find($id->id)->delete();



    }

}
