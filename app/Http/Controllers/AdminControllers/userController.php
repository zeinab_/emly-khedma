<?php

namespace App\Http\Controllers\AdminControllers;
use App\Http\Controllers\Controller;

use App\repositary\userRepositary;
use Illuminate\Http\Request;
use App\Models\User;

class userController extends Controller
{
	protected $model;
	 public function __construct(User $user)
    {
       $this->model = new userRepositary($user);
    }

    public function showuser()
    {
     return view('admininterface.userPage');
    }
    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'firstname' => 'required',
             'lastname' => 'required',
             'password'=>'required|max:8',
             'phone'=>'required',
             'email'=>'required',
             'civilIdImage'=>'required|image|mimes:jpeg,png,svg,jpg,gif|max:1024',

            'img' => 'required|image|mimes:jpeg,png,svg,jpg,gif|max:1024',
        ]);
        
        return $this->model->create($request->all());

    }
  
        public function edit(Request $id)
     {
        
         return response()->json($this->model->find($id->input('id')));
     }
    

}
?>