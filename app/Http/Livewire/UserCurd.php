<?php

namespace App\Http\Livewire;
use App\repositary\userRepositary;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use App\Models\User;

class UserCurd extends Component
{  	use WithFileUploads;

  public $firstname,$lastname,$phone,$img,
	$civilIdImage,$password,$email;
 public $firstname_e,$lastname_e,$phone_e,$img_e,
	$civilIdImage_e,$password_e,$email_e,$old_img,
     $oldcivil,$edit_id,$del_id;

    public function render()
    {   $userData= User::get();
        return view('livewire.user-curd',["data"=>$userData]);
    }
    public function creatUser()
    {
    	
          $imageName = $this->civilIdImage->store('civilphoto','public');
          $imgn= str_replace("civilphoto/","","$imageName");
          // image2
          $image = $this->img->store('personalphoto','public');
          $imgp= str_replace("personalphoto/","","$image");


        User::create([
        	'firstname'=>$this->firstname,
        	'lastname'=>$this->lastname,
        	'email'=>$this->email,
        	'phone'=>$this->phone,
        	'password'=>Hash::make($this->password),
        	'img'=>$imgp,
        	'civilIdImage'=>$imgn
        ]);

    }
    public function edit($id){
    	$user=User::find($id);
    	$this->edit_id=$id;
    	$this->firstname_e=$user->firstname;
    	$this->lastname_e=$user->lastname;
    	$this->phone_e=$user->phone;
    	$this->email_e=$user->email;
    	$this->old_img=$user->img;
    	$this->oldcivil=$user->civilIdImage;

    }
    function update(){
    	$user=User::find($this->edit_id);
    	$validatedData = $this->validate([
            'firstname_e' => 'required',
             'lastname_e' => 'required',
             'phone_e'=>'required',
             'email_e'=>'required',
             
        ]);
      if (isset($this->img_e)) {
        
      
      $oldimg=$this->old_img ;
          if( file_exists ( 'storage/personalphoto/'.$oldimg)===true)
          {  
                  

           unlink(public_path('storage/personalphoto/'.$oldimg));

             $imageName = $this->img_e->store('personalphoto','public');
             $imgn= str_replace("personalphoto/","","$imageName");
             $user->update
             ([
                "img"=>$imgn

             ]);
            }
            elseif( file_exists ( 'storage/personalphoto/'.$oldimg)===false)
          { 
            $imageName = $this->img_e->store('personalphoto','public');
                 $imgn= str_replace("personalphoto/","","$imageName");
                $user->update
                ([
                   "img"=>$imgn

                ]); 
          }
        }
        if(isset($this->civilIdImage_e)){
          $old_civil=$this->oldcivil ;
        
          
        if( file_exists ( 'storage/civilphoto/'.$old_civil)===true)
        {  
                  

          unlink(public_path('storage/civilphoto/'.$old_civil));

            $imageName = $this->civilIdImage_e->store('civilphoto','public');
            $imgs= str_replace("civilphoto/","","$imageName");
            $user->update([
                "civilIdImage"=>$imgs

            ]);
         }
        elseif( file_exists ( 'storage/civilphoto/'.$old_civil)===false)
          { 
            $imageName = $this->civilIdImage_e->store('civilphoto','public');
                 $imgs= str_replace("civilphoto/","","$imageName");
                $user->update
                ([
                   "civilIdImage"=>$imgs

                ]); 
          }
        }
          if(isset($this->password_e))
          {
            $user->update
            ([
              "password"=>Hash::make($this->password_e)
            ]);

          }
          $user->update
          ([
            "firstname"=>$this->firstname_e,
            "lastname"=>$this->lastname_e,
            "email"=>$this->email_e,
            "phone"=>$this->phone_e
          ]);       
    }
    public function delete($id)
    {
      $this->del_id=$id;

    }
    public function del_conf()
    {
      $del=User::find($this->del_id);
      $img=$del->img;
      $imgCivil=$del->civilIdImage;
      unlink(public_path('storage/personalphoto/'.$img)); 
      unlink(public_path('storage/civilphoto/'.$imgCivil)); 
      $del->delete();





    }
    

    
}
