<?php

namespace App\Http\Livewire;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use App\Models\AdminModels\Department;
use Livewire\Component;

class DepartmentCurd extends Component
{
	use WithFileUploads;
	public $name,$name_ar,$img,$ename_ar,
	$name_e,$img_e,$oldimg,$depart_id,$edit_id;

    public function render()
    {
    	$data=Department::get();
        return view('livewire.department-curd',["data"=>$data]);
    }
    public function add_depart()
    {
    	$this->validate([
    		'name'=>'required',
    		'name_ar'=>"required",
    		'img' => 'required|image|mimes:jpeg,png,svg,jpg,gif|max:1024',

    	]);
    	// dd($this->name);
    	$imgu=$this->img->store('departphoto','public');
       $img_name= str_replace("departphoto/","","$imgu");

    	Department::create([
    		'name'   =>$this->name,
    		'name_ar'=>$this->name_ar,
    		'img'    =>$img_name

    	]);


    }
    public function show_depart($id){
    	$depart=Department::find($id);
    	$this->edit_id=$id;
    	$this->ename_ar=$depart->name_ar;
    	$this->name_e=$depart->name;
    	$this->oldimg=$depart->img;


    }
    public function update_depart(){
    	$departs=Department::find($this->edit_id);

    	$this->validate([
    		'name_e'=>'required',
    		'ename_ar'=>"required",
    		

    	]);
    	$departs->update([
    		'name'=>$this->name_e,
    		'name_ar'=>$this->ename_ar
    	]);
    	 // dd($this->img_e);

    	if($this->img_e)
    	{
    		$oldimgn=$this->oldimg;
    		
    		if(file_exists ( 'storage/departphoto/'.$oldimgn)===true)
    		{
    		  unlink(public_path('storage/departphoto/'.$oldimgn));
    		  $newimg=$this->img_e->store('departphoto','public');
    		  $newimg_name=str_replace('departphoto/', "", "$newimg");
    		  $departs->update
    		  ([
    		  	
    		  	'img'=>$newimg_name
    		  ]);
    	    }
    	     else
    	     {
    	     	$newimg=$this->img_e->store('departphoto','public');
    		  $newimg_name=str_replace('departphoto/', "", "$newimg");
    		  $departs->update
    		  ([
    		  	
    		  	'img'=>$newimg_name
    		  ]);


    	     }
    	}
    	

    	

    }
    public function del($id){
    	$this->depart_id=$id;

    }
    public function del_conf(){
    	// dd($this->depart_id);
    	$department=Department::find($this->depart_id);
    	$img=$department->img;
    	unlink(public_path('storage/departphoto/'.$img));
    	$department->delete();

    }
}
