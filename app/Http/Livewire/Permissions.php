<?php

namespace App\Http\Livewire;
use Livewire\Component;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use DB;

class Permissions extends Component
{
	public $name ,$guard_,
	       $name_p ,$guard_p,$edit_r,$delete_role,
         $edit_per,$del_per,$name_edit,$guard_edit,
         $name_edit_r,$guard_edit_r,$rolePermissions,
         $rol_per,$del_id,$del_role_id;
         public $per=[];

    public function render()
    {
        $role=Role::get();
        $permission=Permission::get();
        return view('livewire.permission',compact('role','permission'));
    }
    public function creatRole(){
      
    	$validatedData = $this->validate
    	([
    	  	'name'=>"required",
    	  	'guard_'=>'required'

    	]);

    	$role=Role::create([
    		"name"=>$this->name,
    		"guard_name"=>$this->guard_
    	]);
      foreach ($this->per as $value) {
       $role->givePermissionTo($value);
      }

    }
   public function creatPer()
   {
   	$validatedData = $this->validate
    	([
    	  	'name_p'=>"required",
    	  	'guard_p'=>'required'

    	]);
   	Permission::create
   	([
   		"name"=>$this->name_p,
   		"guard_name"=>$this->guard_p

   	]);

   }
   public function showPermission($id)
   { 
      $this->edit_per=$id;
      $per_data=permission::find($this->edit_per);
      $this->name_edit=$per_data->name;
      $this-> guard_edit=$per_data->guard_name;

   }
   public function editPermission()
   {
    $per_data=permission::find($this->edit_per);

    $validatedData = $this->validate
      ([
          'name_edit'=>"required",
          'guard_edit'=>'required'

      ]);
      $per_data->update
      ([
        'name'=>$this->name_edit,
        'guard_name'=>$this->guard_edit
      ]);

   }

    public function showRole($id)
     { 
      $this->edit_r=$id;
      $role_data=Role::find($this->edit_r);
      $this->rolePermissions=DB::table('role_has_permissions')->where('role_id',$id)->pluck('permission_id');
      $this->rol_per=json_decode($this->rolePermissions);

      $this->name_edit_r=$role_data->name;

      $this-> guard_edit_r=$role_data->guard_name;


     }

     public function editRole()
     {
       $role_data=Role::find($this->edit_r);

       $validatedData = $this->validate
        ([
          'name_edit_r'=>"required",
          'guard_edit_r'=>'required'

         ]);
        $role_data->update
         ([
           'name'=>$this->name_edit_r,
           'guard_name'=>$this->guard_edit_r
          ]);
         $role_data->syncPermissions($this->rol_per);
      }

       public function delPermission($id)
       {
        $this->del_id=$id;
       }
       public function destroy_per()
       {
        $data=Permission::find($this->del_id);
        $data->delete();


       }
       public function delRole($id)
       {
        $this->del_role_id=$id;
       }
       public function destroy_role()
       {
        $data=Role::find($this->del_role_id);
        $data->delete();


       }




}
