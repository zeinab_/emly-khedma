<?php

namespace App\Http\Livewire;
use App\Models\AdminModels\Admin;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use DB;
class AdminCurd extends Component
{ 
	use WithFileUploads;
	public $name,$email,$password,$img,
	       $idVal ,$e_name,$e_password,$e_img,$e_email,$old,$id_del,$role_id,$ed_role_id;
     
    public function render()
    {    $role=Role::get();

    	$admin_data=Admin::get();
        return view('livewire.admin-curd',['admin_data'=>$admin_data,'role'=>$role]);
    }
    public function add_admin(){
    	$validatedData = $this->validate([
            'name' => 'required',
             'email' => 'required',
             'password'=>'required|max:8',

            'img' => 'required|image|mimes:jpeg,png,svg,jpg,gif|max:1024',
        ]);

        $imageName = $this->img->store('adminImage','public');
     

       $imgn= str_replace("adminImage/","","$imageName");


    	$admin=Admin::create([
            "username"=>$this->name,
            "email"=>$this->email,
            "password"=>Hash::make($this->password),
            "img"=>$imgn
        ]);
       $admin->assignRole($this->role_id);


    }
    public function showData($id)
    {
    	$data=Admin::find($id);
        $rolee=DB::table('model_has_roles')->where('model_id',$id)->pluck('role_id');
       
    	$this->idVal=$data->id;
    	$this->e_name=$data->username;
    	$this->e_email=$data->email;
    	$this->old=$data->img;
        foreach ($rolee as $key => $value) {
          
        
       $this->ed_role_id=$rolee[$key];
   }
// dd($this->ed_role_id);
    }
    public function edit_admin()
    {
    	 $edit_info=Admin::find($this->idVal);
        
        if(isset($this->e_img))
        { 
        	$x=$this->old ;
        	
        	if( file_exists ( 'storage/adminImage/'.$x)===true)
        	{  
        		    	

        	 unlink(public_path('storage/adminImage/'.$x));

            $imageName = $this->e_img->store('adminImage','public');
            $imgn= str_replace("adminImage/","","$imageName");
            $edit_info->update([
                "img"=>$imgn

            ]);
            }
            elseif( file_exists ( 'storage/adminImage/'.$x)===false)
        	{ 
        		$imageName = $this->e_img->store('adminImage','public');
                 $imgn= str_replace("adminImage/","","$imageName");
                $edit_info->update
                ([
                   "img"=>$imgn

                ]);	
        	}
        }
            if(isset($this->e_password))
            { dd(
            	 $edit_info->update
            	 ([
                    'password'=>Hash::make($this->e_password)
              ]));
            }

            $edit_info->update
            ([
              'username'=>$this->e_name,
              'email'=>$this->e_email
             ]);
            DB::table('model_has_roles')->where('model_id',$this->idVal)->delete();
    
        $edit_info->assignRole($this->ed_role_id);
    


    }
    public function del($id){
        $this->id_del=$id;

    }
     public function delAdmin() {
     	dd($this->id_del);
        // $del_Admin=Admin::find($this->id_del);
        // $xxx=$del_Admin->img;
        // unlink(public_path('storage/adminImage/'.$xxx)); 

        // $del_Admin->delete();
     }

}
