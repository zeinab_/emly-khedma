<?php 
namespace App\repositary;
use Illuminate\DataBase\Eloquent\Model;
use Illuminate\DataBase\Eloquent\Collection;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;



class userRepositary implements userRepositaryInterface
{
	protected $model;
	public function __construct(Model $model)
	{
		return $this->model =$model;
	}
	public function create(array $data)
	{
		
			 $imgp=$data['img'];

    	$file_exp=$imgp->getClientOriginalExtension();

    	$img_namep=md5(uniqid()).".".$file_exp;

    	$pathp=public_path("/userphoto/personalphoto");

    	$img_place=$imgp->move($pathp,$img_namep);
    	$data['img']=$img_namep;

    	$img=$data['civilIdImage'];
    	 $file_ex=$img->getClientOriginalExtension();

    	 $img_name=md5(uniqid()).".".$file_ex;

    	 $path=public_path("/userphoto/civilphoto");

    	$img_place=$img->move($path,$img_name);
    	 $data['civilIdImage']=$img_name;

		$data['password']=Hash::make($data['password']);
		
		 return $this->model->create($data);

	}
	public function all(){
		return $this->model->all();
	}


	public function find($id){
		return $this->model->find($id);
	}
	// public function update(array $data,$id)
 //    { 
 //    	  // return print_r($data);
 //        $record = $this->model->find($data['id']);
 //        return $record->update($data);
 //    }
 //    public function delete($id){
 //    	return $this->model->destroy($id);
 //    }




}
