<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminControllers\servicesController;
use App\Http\Controllers\AdminControllers\adminInfoController;
use App\Http\Controllers\AdminControllers\userController;
use App\Http\Controllers\AdminControllers\offerController;




Route::prefix('admin')->group(function(){

  
   Route::get('/loginForm',[adminInfoController::class,'showLoginForm'])->name('loginForm');

   Route::post('/loginAdmin',[adminInfoController::class,'Login'])->name('admin.submit');

  Route::middleware('auth:admin')->group(function()
   {
     
  
     Route::get('admins',function(){   
  	   return view('admininterface.adminPage');
      })->name('admin');


  

     Route::get('/permissions',function(){
	   return view('admininterface.permissionpage');
      });

      // Route::get('/user',[userController::class,'showuser']);
     Route::get('/users',function(){
     return view('admininterface.user');
     });
     Route::post('/adduser',[userController::class,'create'])->name('adduser');

     Route::get('/edituser',[userController::class,'edit'])->name('edit.user');

 
     Route::get('/departments',function(){
     return view('admininterface.departmentPage');
     });
     Route::get('/services',[servicesController::class,'servicePage'])->name('services');
     Route::post('/addservice',[servicesController::class,
     'add_service'])->name('service');
     Route::post('/showserve',[servicesController::class,'show_service'])->name('show.service');
     Route::put('/updateserve',[servicesController::class,'update_service'])->name('update.service');
     Route::post('/delete',[servicesController::class,'delete_service'])->name('del');
     Route::get('/offers',[offerController::class,'showOfferPage']);
     Route::post('/addoffer',[offerController::class,'offer_add'])->name('offer.add');
     Route::post('editoffer',[offerController::class,'offer_show'])->name('offer.edit');
     Route::put('/updateoffer',[offerController::class,'offer_update'])->name('offer.update');
     Route::post('/deloffer',[offerController::class,'delete_offer'])->name('delete.offer');
     // Route::get('/euser',[userController::class,'xx']);
    });
});





