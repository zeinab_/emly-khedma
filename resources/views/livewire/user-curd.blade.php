<div>
    <div  class="page-wrapper" style="min-height: 250px;" >
            
            <div class="page-breadcrumb bg-white">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">ADMIN</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <div class="d-md-flex">
                            <ol class="breadcrumb ms-auto">
                                <li><a href="#" class="fw-normal">Dashboard</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container-fluid">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                           <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" id="addUsers">
                           <i class="fas fa-plus" ></i>ADD User 
                            </button> 
                            <table class="table table-striped">
                              <thead>
                                <th>firstname</th>
                                <th>lastname</th>
                                <th>email</th>
                                <th>personalimage</th>
                                <th>civilimage</th>
                                <th>controls</th>
                              </thead>
                              <tbody>
                                @foreach($data as $data)
                                <tr>
                                  <td>{{$data->firstname}}</td>
                                  <td>{{$data->lastname}}</td>
                                  <td>{{$data->email}}</td>
                                  <td><img width="50px" height="50px" src="{{asset('storage/personalphoto/'.$data->img)}}"/></td>
                                  <td><img width="50px" height="50px" src="{{asset('storage/civilphoto/'.$data->civilIdImage)}}"/></td>
                                  <td><button class="fas fa-pencil-alt ed_us" wire:click="edit({{$data->id}})"></button>
                                    <button wire:click="delete({{$data->id}})" class="fas fa-trash-alt delete_user"></button></td>
                                </tr>
                                @endforeach
                              </tbody>
                              
                            </table>


                            </div>
                    </div>
                </div>
                
            </div>
          </div>

          <!-- deletemodel -->
<div wire:ignore.self class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Do you want delete user?</p>
      </div>
      <div class="modal-footer">
        <button type="submit" wire:click="del_conf" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

          
<!-- edit Modal -->
<div wire:ignore.self class="modal fade" id="editusers" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">edit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form wire:submit.prevent="update" enctype="multipart/form-data">
          @csrf
          @method('PUT') 
<input type="hidden" name="id" wire:model="edit_id">
  <div class="form-group">
    <label for="exampleInputname">First name</label>
    <input type="text" class="form-control fn" id="firstname" aria-describedby="emailHelp"  placeholder="Enter name" wire:model="firstname_e">
   
 </div>
  <div class="form-group">
    <label for="exampleInputlast1">Last Name</label>
    <input type="text" class="form-control" id="lastname" placeholder="Last Name" wire:model="lastname_e">
  </div>

  <div class="form-group">
    <label for="exampleInputphone">phone</label>
    <input type="text" class="form-control" id="phone"  placeholder="Enter phone" wire:model="phone_e">
    
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="password" placeholder="Password" wire:model="password_e">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" wire:model="email_e">
    </div>
    <div class="form-group">
  	<img src="{{asset('storage/personalphoto/'.$old_img)}}" width="70px" height="70px" wire:model="old_img" />
  </div>
  <div class="form-group">
    <label for="exampleInputimg1">img</label>
    <input type="file" class="form-control" id="img" wire:model="img_e">
  </div>
  <div class="form-group">
  	<img src="{{asset('storage/civilphoto/'.$oldcivil)}}" width="70px" height="70px" wire:model="oldcivil" />
  </div>
  <div class="form-group">
    <label for="exampleInputimgid">civilIdImage</label>
    <input type="file" class="form-control" id="civilimg" wire:model="civilIdImage_e" >
  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary editt" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
    </form>
      </div>
    </div>
  </div>
</div>







<!-- livewire -->
</div>
<!--end of livewire -->

<div wire:ignore.self class="modal fade" id="exampleModalll" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form wire:submit.prevent="creatUser" enctype="multipart/form-data">
        	@csrf
  <div class="form-group">
    <label for="exampleInputname">First name</label>
    <input type="text" class="form-control" id="exampleInputname" aria-describedby="emailHelp" placeholder="Enter name" wire:model="firstname">
    @error('firstname')
    <span class="alert alert-danger">{{ $message }}</span>
   @enderror
 </div>
  <div class="form-group">
    <label for="exampleInputlast1">Last Name</label>
    <input type="text" class="form-control" id="exampleInputlast1" placeholder="Last Name" wire:model="lastname">
  </div>

  <div class="form-group">
    <label for="exampleInputphone">phone</label>
    <input type="text" class="form-control" id="exampleInputphone1"  placeholder="Enter phone" wire:model="phone">
    
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" wire:model="password">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" wire:model="email">
    </div>
    
  <div class="form-group">
    <label for="exampleInputimg1">img</label>
    <input type="file" class="form-control" id="exampleInputimg1" wire:model="img">
  </div>
  
  <div class="form-group">
    <label for="exampleInputimgid">civilIdImage</label>
    <input type="file" class="form-control" id="exampleInputimgid" wire:model="civilIdImage" >
  </div>

  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit"  class="btn btn-primary user">Save changes</button>
    </form>
      </div>
    </div>
  </div>
</div>



