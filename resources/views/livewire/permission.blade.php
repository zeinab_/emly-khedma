<div>
    <div  class="page-wrapper" style="min-height: 250px;" >
            
            <div class="page-breadcrumb bg-white">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">ADMIN</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <div class="d-md-flex">
                            <ol class="breadcrumb ms-auto">
                                <li><a href="#" class="fw-normal">Dashboard</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="white-box">
                        	<button class="btn btn-primary role">ADD ROLE</button>
                          <table class="table table-striped">
                            <thead>
                              <th>name</th>
                              <th> controles</th>

                            </thead>
                            <tbody>
                              @foreach($role as $role)
                              <tr>
                                
                                <td> {{$role->name}}</td>
                                <td> <button class="fas fa-pencil-alt edit_Role" wire:click="showRole({{$role->id}})"></button>
                                  <button class="fas fa-trash-alt delete_role" id_role="{{$role->id}}" wire:click="delRole({{$role->id}})"></button>
                                </td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>


                           </div>
                    </div>

                  <div class="col-md-6">
                     <div class="white-box">

                    <button class="btn btn-primary permission">ADD Permisson</button>
                    <table class="table table-striped">
                            <thead>
                              <th>name</th>
                              <th> controles</th>

                            </thead>
                            <tbody>
                              @foreach($permission as $per)
                              <tr>
                                
                                <td> {{$per->name}}</td>
                                <td> <button class="fas fa-pencil-alt edit_permission" wire:click="showPermission({{$per->id}})">
                                </button>
                                  <button class="fas fa-trash-alt" id_per="{{$per->id}}" wire:click=" delPermission({{$per->id}})"></button>
                                </td>
                              </tr>
                              @endforeach
                            </tbody>
                          </table>
                  </div>
                </div>
                </div>
                
            </div>
          </div>

<div wire:ignore.self class="modal fade" id="role" tabindex="-1"     role="dialog" aria-labelledby="exampleModalCenterTitle"         aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">ADD Role</h5>
        
      </div>
      <div class="modal-body">
        <form method="POST" wire:submit.prevent="creatRole" >
          <div class="form-group">
    <label for="example">name</label>
    <input type="text" class="form-control" id="example" aria-describedby="emailHelp" wire:model="name">
    </div>
  <div class="form-group">
    <label for="guard">guard name</label>
    <input type="text" class="form-control" id="guard" wire:model="guard_">
  </div>
<h4>permissions</h4>
  @php 
  use Spatie\Permission\Models\Permission;
   $per=Permission::get()->pluck('name','id');

   foreach($per as $key=>$row){

   @endphp
   <div class="form-check">
  <input class="form-check-input" type="checkbox" value="{{$key}}" id="flexCheckDefault" wire:model="per">
  <label class="form-check-label" for="flexCheckDefault">
    {{$row}}
  </label>
</div>
@php
 }
   @endphp
   
  <button type="submit" class="btn btn-primary">Submit</button>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>


<!--  -->
<div wire:ignore.self class="modal fade" id="permission" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">ADD Permission</h5>
        
      </div>
      <div class="modal-body">
        <form method="POST" wire:submit.prevent="creatPer" >
          <div class="form-group">
    <label for="example">name</label>
    <input type="text" class="form-control" id="example" aria-describedby="emailHelp" wire:model="name_p">
    </div>
  <div class="form-group">
    <label for="guard">guard name</label>
    <input type="text" class="form-control" id="guard" wire:model="guard_p">
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- edit permission -->
<div wire:ignore.self class="modal fade" id="edit_permission" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">EDIT Permission</h5>
        
      </div>
      <div class="modal-body">
        <form method="POST" wire:submit.prevent="editPermission" >
          <div class="form-group">
    <label for="example">name</label>
    <input type="text" class="form-control" id="example" aria-describedby="emailHelp" wire:model="name_edit">
    </div>
  <div class="form-group">
    <label for="guard">guard name</label>
    <input type="text" class="form-control" id="guard" wire:model="guard_edit">
  </div>
  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
      </div>
    </div>
  </div>
  <!-- edit role -->
  <div wire:ignore.self class="modal fade" id="edit_Role" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">EDIT Role</h5>
        
      </div>
      <div class="modal-body">
        <form method="POST" wire:submit.prevent="editRole" >
          <div class="form-group">
    <label for="example">name</label>
    <input type="text" class="form-control" id="example" aria-describedby="emailHelp" wire:model="name_edit_r">
    </div>
  <div class="form-group">
    <label for="guard">guard name</label>
    <input type="text" class="form-control" id="guard" wire:model="guard_edit_r">
  </div>

@foreach($permission as $p)

<div class="form-check">
  <input class="form-check-input" type="checkbox" value="{{$p->id}}" id="flexCheckDefault" wire:model="rol_per" />
   <label class="form-check-label" for="flexCheckDefault">
    {{$p->name}}
     </label>
   
</div>
@endforeach
 </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    
      </div></div></div></div>
      <!-- delpermission -->

<div wire:ignore.self class="modal fade" id="delete_Per" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">delete Permission</h5>
        
      </div>
      <div class="modal-body">
        Do you want delete permission?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" wire:click="destroy" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- role delete -->

<div wire:ignore.self class="modal fade" id="delete_role" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">delete Role</h5>
        
      </div>
      <div class="modal-body">
        Do you want delete role?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" wire:click="destroy_role" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
</div>
