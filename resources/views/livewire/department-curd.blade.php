
<div>


<div  class="page-wrapper" style="min-height: 250px;" >
            
            <div class="page-breadcrumb bg-white">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{__("site.admin")}}</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <div class="d-md-flex">
                            <ol class="breadcrumb ms-auto">
                                <li><a href="#" class="fw-normal">{{__("site.dashboard")}}</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container-fluid">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                           <button class="btn btn-primary add_depart" >
                            <i class="fas fa-plus" ></i>{{__('site.add')}} {{__('site.department')}} 
                            </button> 
                            <table class="table table-stripped">
                            	<thead>
                            		<th>{{__('site.name')}} {{__('site.department')}} {{__('site.en')}}</th>
                            		<th>{{__('site.name')}} {{__('site.department')}} </th>
                            		<th>{{__('site.img')}}</th>
                            		<th>{{__('site.controls')}}</th>
                            	</thead>
                            	<tbody>
                            		@foreach($data as $data )
                            		<tr>
                            		<td>{{$data->name}}</td>
                            		<td>{{$data->name_ar}}</td>
                            		<td><img width="50px" height="50px" src="{{asset('storage/departphoto/'.$data->img)}}"></td>
                            		
                            			<td><button class="fas fa-pencil-alt depart_edit " wire:click="show_depart({{$data->id}})"></button>
                                       <button wire:click="del({{$data->id}})" class="fas fa-trash-alt del_depart "></button></td>
                            		</tr>
                            		@endforeach
                            	</tbody>
                            </table>


                        </div>
                    </div>
                </div>
                
            </div>
           <!--add  -->
<div wire:ignore.self class="modal fade" id="depart_add" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{__('site.add')}} {{__('site.department')}}</h5>
        
      </div>
      <div class="modal-body">
  <form enctype="multipart/form-data" wire:submit.prevent="add_depart">
  @csrf 
  	<div class="form-group">
    <label for="exampleInputname">{{__('site.name')}} {{__('site.department')}} {{__('site.en')}} </label>
    <input type="text" class="form-control" id="exampleInputname" aria-describedby="emailHelp" placeholder="{{__('site.enter')}} {{__('site.name')}} {{__('site.department')}}" wire:model="name">
  </div>
  <div class="form-group">
    <label for="exampleInputnamear">{{__('site.name')}} {{__('site.department')}} </label>
    <input type="text" class="form-control" id="exampleInputnamear" aria-describedby="emailHelp" placeholder="{{__('site.enter')}} {{__('site.name')}} {{__('site.department')}}" wire:model="name_ar">
  </div>
  <div class="form-group">
    <label for="exampleInputnameimg">{{__('site.img')}}</label>
    <input type="file" class="form-control" id="exampleInputnameimg" aria-describedby="emailHelp" placeholder="Enter department name" wire:model="img">
  </div>


  <button type="button" class="btn btn-secondary close" data-dismiss="modal">{{__('site.close')}}</button>
          <button type="submit" class="btn btn-primary submit" >{{__('site.save')}}</button>
 </form>
    </div>
  </div>
</div>
</div>

<!--edit  -->
     <div wire:ignore.self class="modal fade" id="depart_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{__('site.edit')}} {{__('site.department')}}</h5>
        
      </div>
      <div class="modal-body">
      	 <form enctype="multipart/form-data" wire:submit.prevent="update_depart" >
      	 	
        <div class="form-group">
    <label for="examplename">{{__('site.name')}} {{__('site.department')}} {{__('site.en')}}</label>
    <input type="text" class="form-control" id="examplename" aria-describedby="emailHelp" placeholder="Enter department name" wire:model="name_e">
  </div>
  <div class="form-group">
    <label for="examplenamear">{{__('site.name')}} {{__('site.department')}}</label>
    <input type="text" class="form-control" id="examplenamear" aria-describedby="emailHelp" placeholder="Enter department name" wire:model="ename_ar">
  </div>
  <div class="form-group">
    <label for="examplenameimg">{{__('site.img')}}</label>
    <input type="file" class="form-control" wire:model="img_e">
  </div>

  <button type="button" class="btn btn-secondary close" data-dismiss="modal">{{__('site.close')}}</button>
          <button type="submit"  class="btn btn-primary submit" >{{__('site.save')}}</button>

      
 
      </form>
      
    </div>
  </div>
</div>
</div>
 
 <!--  -->



 
	<!--  -->
</div>
<!--  -->


<!--del  -->
<div wire:ignore.self class="modal fade" id="depart_del" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{__('site.delete')}} {{__('site.department')}}</h5>
        
      </div>
      <div class="modal-body">
   {{__('site.do you want')}} {{__('site.delete')}} {{__('site.department')}}?
      <div class="modal-footer">

 <button type="submit" wire:click="del_conf" class="btn btn-primary" >{{__('site.save')}}</button>

  <button type="button" class="btn btn-secondary close" data-dismiss="modal">{{__('site.close')}}</button>
      
      </div>
    </div>
  </div>
</div>





    


       
     
