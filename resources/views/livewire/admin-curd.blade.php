<div>
   <div  class="page-wrapper" style="min-height: 250px;" >
            
            <div class="page-breadcrumb bg-white">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">ADMIN</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <div class="d-md-flex">
                            <ol class="breadcrumb ms-auto">
                                <li><a href="#" class="fw-normal">Dashboard</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="container-fluid">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                           <button class="btn btn-primary add_admin" >
                            <i class="fas fa-plus" ></i>ADD 
                            </button>  
                            

                            
                             <table class="table table-striped">
	                           <thead>
		                        <th>name</th>
		                        <th> email</th>
		                        <th>image</th>
		                        <th>controls</th>
	                           </thead>

	                           <tbody>
		                        @foreach($admin_data as $row)
		                        <tr>
			                        <td>{{$row->username}}</td>
			                        <td>{{$row->email}}</td>
			                        <td><img src="{{asset('storage/adminImage/'.$row->img)}}" width="50px" height="50px" /></td>
			                       <td>
                              
                                     <i wire:click="showData({{$row->id}})" class="fas fa-pencil-alt btn_edit"  style=" margin-right: 25px;
                                     margin-left: 10px;"  id_e_u="{{$row->id}}" ></i>
                                     
                                     <i class="fas fa-trash-alt delete_admin" wire:click="del({{$row->id}})" style="color: black"
                                     	>
                                   	
                                    </i>
                                 </td>
		                       </tr>
		                       @endforeach
	                          </tbody>
                            </table>

 
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- add model -->
            
 <div wire:ignore.self class="modal fade" id="models" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">ADD ADMIN</h5>
        
      </div>
      <div class="modal-body">
  <form enctype="multipart/form-data" wire:submit.prevent="add_admin">
  	<div class="form-group">
    <label for="exampleInputEmail1">username</label>
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter username" wire:model="name">
  </div>
  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
     <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter your email" wire:model="email">
  </div>
  @error('email')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label for="exampleInputEmail1">password</label>
    <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Enter password" wire:model="password">
  </div>
  @error('password')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label >image</label>
    <input type="file" class="form-control" wire:model="img">
  </div>
  @error('img')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
   <select class="form-select" wire:model="role_id" aria-label="Default select example">
      <option ></option>
@foreach($role as $role)
  <option value="{{$role->id}}" >{{$role->name}}</option>
 @endforeach
</select>
  </div>
<button type="button" class="btn btn-secondary close" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary submit" >Submit</button>

      
 </form>
      
      
    </div>
  </div>
</div>
</div>
</div>
<!-- edit admin data -->
<div wire:ignore.self class="modal fade" id="model" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit ADMIN</h5>
        
      </div>
      <div class="modal-body">
  <form enctype="multipart/form-data" wire:submit.prevent="edit_admin">
  	<input type="hidden" name="id" wire:model="idVal">

  	<div class="form-group">
    <label for="exampleInputEmail1">username</label>
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter username" wire:model="e_name">
  </div>
     <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter your email" wire:model="e_email">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">password</label>
    <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Enter password" wire:model="e_password">
  </div>
  <div class="form-group">
  	<img src="{{asset('storage/adminImage/'.$old)}}" width="70px" height="70px" wire:model="old" />
  </div>
  <div class="form-group">
    <label >image</label>
    <input type="file" class="form-control" wire:model="e_img">
  </div>
  <select class="form-select" wire:model="ed_role_id" aria-label="Default select example">
      <option ></option>
      @php
      use Spatie\Permission\Models\Role;
      $role=Role::get();

foreach($role as $role){
 @endphp

  <option value="{{$role->id}}" >{{$role->name}}</option>
  @php
}
 @endphp
</select>
  

        <button type="button" class="btn btn-secondary cancel" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary ok" >Submit</button>

      
 </form>
      
      
    </div>
  </div>
</div>
</div>
</div>
<!-- delete Model -->


<div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top: 100px">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">delete</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
       Are you sure delete this?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">no</button>
        <button type="button" wire:click="delAdmin" class="btn btn-primary">yes</button>
      </div>
    </div>
  </div>
</div>    
 
</div>
