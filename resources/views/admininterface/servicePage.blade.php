@include('admininterface.includes.header')
<div  class="page-wrapper " style="min-height: 250px;" >
            
            <div class="page-breadcrumb bg-white">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{__("site.admin")}}</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <div class="d-md-flex">
                            <ol class="breadcrumb ms-auto">
                                <li><a href="#" class="fw-normal">{{__("site.dashboard")}}</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
            </div>
            <?php
                use App\Models\User;
                use App\Models\AdminModels\Department;

                ?>

            <div class="container-fluid">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                        	<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">
                           <i class="fas fa-plus" ></i>{{__('site.add')}} {{__('site.service')}}
                            </button>
                            <table class="table">
                            	<thead>
                            		<th>{{__('site.name')}} {{__('site.service')}}</th>
                            		<th>{{__('site.description')}}</th>
                            		<th>{{__('site.start date')}}</th>
                            		<th>{{__('site.finish date')}}</th>
                            		<th>{{__('site.place')}}</th>
                            		<th>{{__('site.name')}} {{__('site.department')}} </th>
                            		<th>{{__('site.name')}} {{__('site.user')}} </th>
                            		<th>{{__('site.status')}}</th>

                            		<th>{{__('site.controls')}}</th>
                            	</thead>
                            	<tbody>
                            		@foreach($services as $services)
                            		<tr>
                            			<td>{{$services->name}}</td>
                            			<td>{{$services->description}}</td>
                            			<td>{{$services->date_of_start}}</td>
                            			<td>{{$services->date_of_end}}</td>
                            			<td>{{$services->place}}</td>
                            			<td>
                            				<?php

                            				 $departs=Department::get()->where('id',$services->department_id);
                            				 foreach ($departs as $departs) {
                            				     echo $departs->name_ar;
                            				 }
                            			    ?></td>
                            			<td>
                            				<?php

                            				 $er=User::get()->where('id',$services->user_id);
                            				 foreach ($er as $er) {
                            				     echo $er->firstname;
                            				 }
                            			    ?>
                            		    </td>
                            			<td>{{$services->status}}</td>
                            			<td>
                            				<button class="fas fa-pencil-alt serve_edit" serv_edit="{{$services->id}}" data-toggle="modal" data-target=".bd-example-modal-lgg"></button>
                                       <button class="fas fa-trash-alt serve_del" serv_del="{{$services->id}}"
                                        data-toggle="modal" data-target=".bd-example-modal-lggg"></button></td>
                            	    </tr>
                            	    @endforeach
                            	</tbody>
                            </table>


                           </div>
                    </div>
                </div>
                
            </div>


<!-- add -->

<div class="modal fade bd-example-modal-lg" style="padding-top: 30px;" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
       <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{__('site.add')}} {{__('site.service')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<form id="form" method="POST" name="form" enctype="multipart/form-data">
      		@csrf


      	  <div class="form-group">
      	  	<label>{{__('site.name')}} {{__('site.service')}} </label>
      	  	<input type="text" name="name" class="form-control name">

      	  </div>
      	  <div class="form-group">
      	  	<label>{{__('site.description')}}</label>
      	  	<input type="text" name="desc" class="form-control desc">

      	  </div>
      	  <div class="form-group">
      	  	<label>{{__('site.start date')}}</label>
      	  	<input type="date" name="start" class="form-control">

      	  	
      	  </div>
      	  <div class="form-group">
      	    <label>{{__('site.finish date')}}</label>
      	    <input type="date" name="end" class="form-control">


      	  </div>
      	  <div class="form-group">
      	    <label>{{__('site.place')}}</label>
      	    <input type="text" name="place" class="form-control">



      	  </div>


      	  <div class="form-group">
      	    <label>{{__('site.department')}}</label>
      	      <select class="form-select" name="depart">
      	      	<option></option>
      	      	@foreach($depart as $depart)
      	    	<option value="{{$depart->id}}">{{$depart->name_ar}}</option>
      	    	@endforeach
      	    </select>


      	  </div>
      	  <div class="form-group">
      	    <label>{{__('site.user')}} {{__('site.name')}}</label>
      	    <select class="form-select" name="user">
      	    	<option></option>
      	    	@foreach($user as $user)
      	    	<option value="{{$user->id}}">{{$user->firstname}}</option>
      	    	@endforeach
      	    </select>


      	  </div>


      </div>
      <div class="modal-header">
      	 <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('site.close')}}</button>
        <button type="submit" class="btn btn-primary add_service">{{__('site.save')}}</button>

      </div>
             </form>

    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lgg" style="padding-top: 30px;" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
       <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{__('site.edit')}} {{__('site.service')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<form id="form_edit" method="POST" name="form_edit" enctype="multipart/form-data">
      		@csrf
          @method('PUT') 

      		<input type="hidden" name="edit_s" class="edit_s">

      	  <div class="form-group">
      	  	<label>{{__('site.name')}} {{__('site.service')}} </label>
      	  	<input type="text" name="name_e" class="form-control name">

      	  </div>
                    <span class="text-danger valid"></span>

      	  <div class="form-group">
      	  	<label>{{__('site.description')}}</label>
      	  	<input type="text" name="desc_e" class="form-control desc">

      	  </div>
                    <span class="text-danger valid"></span>

      	  <div class="form-group">
      	  	<label>{{__('site.start date')}}</label>
      	  	<input type="date" name="start_e" class="form-control start_e">
      	  </div>


      	  <div class="form-group">
      	    <label>{{__('site.finish date')}}</label>
      	    <input type="date" name="end_e" class="form-control end_e">
      	  </div>

      	  <div class="form-group">
      	    <label>{{__('site.place')}}</label>
      	    <input type="text" name="place_e" class="form-control place_e">

      	  </div>
                    <span class="text-danger valid"></span>


      	  <div class="form-group">
      	    <label>{{__('site.department')}}</label>
      	      <select class="form-control depart_e" name="depart_e">
      	      	<option></option>
      	      	@php $data_d=Department::get(); 
      	      	@endphp
      	      	@foreach($data_d as $data_d)
      	      	<option value="{{$data_d->id}}">{{$data_d->name_ar}}</option>

      	      	@endforeach
      	      	
      	    </select>


      	  </div>
      	  <div class="form-group">
      	    <label>{{__('site.user')}} {{__('site.name')}}</label>
      	    <select class="form-control user_e" name="user_e">
      	    	<option>choose user</option>
      	    	@php $data_u=User::get(); 
      	      	@endphp
      	      	@foreach($data_u as $data_u)
      	      	<option value="{{$data_u->id}}">{{$data_u->firstname}}</option>

      	      	@endforeach
      	    	
      	    </select>
 

      	  </div>
      </div>
      <div class="modal-header">
      	 <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('site.close')}}</button>
        <button type="button" class="btn btn-primary edit_service">{{__('save changes')}}</button>

      </div>
    </form>

    </div>
  </div>
</div>

<!-- delete -->
<div class="modal fade bd-example-modal-lggg" style="padding-top: 30px;" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
       <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">{{__('site.delete')}} {{__('site.service')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{__('site.do you want')}} {{__('site.delete')}} {{__('site.this')}} {{__('site.service')}}?
        </div>
        <div class="modal-header">
         <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('site.close')}}</button>
        <button type="submit" class="btn btn-primary del_service">{{__('site.save')}}</button>

      </div>

    </div>
  </div>
</div>



@include('admininterface.includes.footer')
<!-- edit -->
