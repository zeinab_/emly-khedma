@include('admininterface.includes.header')

@php 
use App\Models\UserModels\Service;
use App\Models\User;
$ser=Service::get();
$user=User::get();
$ser_e=Service::get();
$user_e=User::get();

@endphp


<div  class="page-wrapper" style="min-height: 250px;" >
            
            <div class="page-breadcrumb bg-white">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{__("site.admin")}}</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <div class="d-md-flex">
                            <ol class="breadcrumb ms-auto">
                                <li><a href="#" class="fw-normal">{{__("site.dashboard")}}</a></li>
                            </ol>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                        	<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-llg">
                           <i class="fas fa-plus" ></i>{{__("site.add")}} {{__("site.offer")}}
                            </button>
                            <table class="table table-striped">
                            	<thead>
                            		<th>{{__('site.price')}}  {{__('site.hour')}}</th>
                            		<th>{{__('site.number')}} {{__('site.hours')}} </th>
                            		<th>{{__('site.service')}}</th>
                            		<th>{{__('site.user')}}</th>
                            		<th>{{__('site.controls')}}</th>
                            	</thead>
                            	<tbody>
                            		@foreach($offer as $of)
                            		<tr>
                            			<td>{{$of->price_of_hour}}</td>
                            			<td>{{$of->hours}}</td>
                            			<td>
                            				<?php 
                            				$name_s=Service::get()->where('id',$of->service_id);
                            			
                            			foreach($name_s as $name){
                            			echo $name->name;
                            			}
                            			?></td>
                            			<td><?php 
                            				$name_u=User::get()->where('id',$of->user_id);
                            			
                            			foreach($name_u as $name){
                            			echo $name->firstname;
                            			}
                            			?>

                            		</td>
                            			
                            			<td>
                            				<button class="fas fa-pencil-alt offer_edit" data-bs-toggle="modal" 
                            				edit_offer="{{$of->id}}"data-bs-target="#exampleModal"></button>
                            				
                                       <button class="fas fa-trash-alt offer_del" offer_del="{{$of->id}}"
                                        data-bs-toggle="modal" data-bs-target="#exampleModall"></button>
                            			</td>
                            		</tr>
                            		@endforeach
                            	</tbody>

                            </table>

                           </div>
                    </div>
                </div>
                
            </div>
            <!-- add Offer -->
    <div class="modal fade bd-example-modal-llg" style="padding-top: 30px;" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
       <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">{{__('site.add')}} {{__('site.offer')}}</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<form id="offers" method="POST" name="offers" enctype="multipart/form-data">
      		@csrf


       <div class="form-group">
      	  	<label>{{__("site.price")}} {{__("site.hour")}}</label>
      	  	<input type="text" name="hour" class="form-control hour">
      	  <div class="alert-danger" id="hourn"></div>

      	</div>
      	<div class="form-group">

      	  <label>{{__('site.number')}} {{__('site.hours')}}</label>
      	  	<input type="text" name="hours" class="form-control hours">
      	  	<div class="alert-danger" id="hoursn"></div>

      	  </div>
      	 <div class="form-group">

      	  <label>{{__('site.user')}} {{__('site.name')}}</label>

      	  	<select name="user" class="form-select " >
      	  		<option></option>
      	  		@foreach($user as $user)
      	  		<option value="{{$user->id}}">{{$user->firstname}}</option>
      	  		@endforeach
      	  	</select>
      	  	 <div class="alert-danger" id="user"></div>

      	  </div>
      	  <div class="form-group">
      	  <label>{{__('site.name')}} {{__('site.service')}}</label>
      	  	<select name="service" class="form-select">
      	  		      	  <option></option>

      	  		@foreach($ser as $ser)
      	  		<option value="{{$ser->id}}">{{$ser->name}}</option>
      	  		@endforeach
      	  	</select>
      	  	<div class="alert-danger" id="service"></div>

      	  </div>

      	  <div class="modal-footer">
      	 <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('site.close')}}</button>
        <button type="submit" class="btn btn-primary add_offer" >{{__('site.save')}}</button>

      </div>
      </form>

    </div>
  </div>
</div>


@include('admininterface.includes.footer')
<!-- edit -->
<div class="modal fade " id="exampleModal" style="padding-top: 30px;" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
       <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">{{__('site.edit')}} {{__('site.offer')}}</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<form id="offer_edit" method="POST" name="offers_edit" enctype="multipart/form-data">
      @csrf
        @method('PUT')
        <input type="hidden" name="id" class="update_id">
       <div class="form-group">
      	  	<label>{{__("site.price")}} {{__("site.hour")}}</label>
      	  	<input type="text" name="hour_e" class="form-control hour_e">
      	  <div class="alert-danger" id="hour_e"></div>

      	</div>
      	<div class="form-group">

      	  <label>{{__('site.number')}} {{__('site.hours')}}</label>
      	  	<input type="text" name="hours_e" class="form-control hours_e">
      	  	<div class="alert-danger" id="hoursn_e"></div>

      	  </div>
      	 <div class="form-group">

      	  <label>{{__('site.user')}} {{__('site.name')}}</label>

      	  	<select name="users_e" class="form-select user_e " >
      	  		<option></option>
      	  		@foreach($user_e as $user_e)
      	  		<option value="{{$user_e->id}}">{{$user_e->firstname}}</option>
      	  		@endforeach
      	  	</select>
      	  	 <div class="alert-danger" id="user_e"></div>

      	  </div>
      	  <div class="form-group">
      	  <label>{{__('site.name')}} {{__('site.service')}}</label>
      	  	<select name="service_e" class="form-select service_e">
      	  		      	  <option></option>

      	  		@foreach($ser_e as $ser_e)
      	  		<option value="{{$ser_e->id}}">{{$ser_e->name}}</option>
      	  		@endforeach
      	  	</select>
      	  	<div class="alert-danger" id="service_e"></div>

      	  </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{__('site.close')}}</button>
        <button type="button" class="btn btn-primary offer_update">{{__('site.save')}}</button>
      </div>
    </div>
  </div>
</div>
<!-- del -->
<div class="modal fade " id="exampleModall" style="padding-top: 30px;" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
       <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle"> {{__('site.delete')}} {{__('site.offer')}}</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	{{__('site.do you want')}} {{__('site.delete')}} {{__('site.this')}} {{__('site.offer')}}?
      	</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{__('site.close')}}</button>
        <button type="button" class="btn btn-primary offer_delete">{{__('site.save')}}</button>
      </div>
    </div>
  </div>
</div>
