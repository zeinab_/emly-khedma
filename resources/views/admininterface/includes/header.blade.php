<!DOCTYPE html>
<html >

<head>

    <meta charset="utf-8">
    
    <title>Ample Admin Lite Template by WrapPixel</title>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
 -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('adminfile/plugins/images/favicon.png')}}">
   <link href="{{asset('adminfile/css/style.min.css')}}" rel="stylesheet">
   @if(url()->current()==="http://localhost:8000/admin/services"|| url()->current()==="http://localhost:8000/admin/offers"||url()->current()==="http://localhost:8000/admin/departments")
   <style type="text/css">
       .left-sidebar{
        right: 0;

       }
       .navbar{
        direction: rtl;
       }
       .ms-auto{
        direction: rtl;
        position: absolute;
        left:0;
       }
       #main-wrapper[data-layout=vertical][data-sidebartype=full] .page-wrapper{
        padding-right: 240px;
        margin-left: 0px;
       }
       .page-breadcrumb {
        padding-left: 90%;

       }

       .page-breadcrumb .page-title{
        padding-left: 90%}
        .breadcrumb{top:0; margin-left: 10px} 
        .page-wrapper>.container-fluid{
            direction: rtl;
        }
        .modal.fade .modal-dialog{
            direction: rtl;
        }
        .sidebar-nav{
            direction: rtl;
        }

   </style>

   @endif
            @livewireStyles


</head>

<body>
    
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
        data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
        
        <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin6">
                    
                    <a class="navbar-brand" href="dashboard.html">
                        <!-- Logo icon -->
                        <b class="logo-icon">
                            <!-- Dark Logo icon -->
                            <img src="{{asset('adminfile/plugins/images/logo-icon.png')}}" alt="homepage" />
                        </b>
                 
                        <span class="logo-text">
                            <!-- dark Logo text -->
                            <img src="{{asset('adminfile/plugins/images/logo-text.png')}}" alt="homepage" />
                        </span>
                    </a>
                    
                    <a class="nav-toggler waves-effect waves-light text-dark d-block d-md-none"
                        href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                </div>
                
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                    <ul class="navbar-nav d-none d-md-block d-lg-none">
                        <li class="nav-item">
                            <a class="nav-toggler nav-link waves-effect waves-light text-white"
                                href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                        </li>
                    </ul>
                     <ul class="navbar-nav ms-auto d-flex align-items-center">

                        
                        <li class=" in">
                            <form role="search" class="app-search d-none d-md-block me-3">
                                <input type="text" placeholder="Search..." class="form-control mt-0">
                                <a href="" class="active">
                                    <i class="fa fa-search"></i>
                                </a>
                            </form>
                        </li>
                        
                        <li>
                            @php 
                            $admin=session()->get('id');

                            @endphp 
                            <a class="profile-pic" href="#">

                                <img src="{{asset('storage/adminImage/'.$admin['img'])}}" alt="user-img" width="36"
                                    class="img-circle"><span class="text-white font-medium">{{$admin['username']}}</span></a>
                        </li>
                        
                    </ul>
                </div>
            </nav>
        </header>
          <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <!-- User Profile-->
                        <li class="sidebar-item pt-2">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashboard.html"
                                aria-expanded="false">
                                <i class="far fa-clock" aria-hidden="true"></i>
                                <span class="hide-menu">@if(url()->current()==="http://localhost:8000/admin/services"|| url()->current()==="http://localhost:8000/admin/offers"||url()->current()==="http://localhost:8000/admin/departments")
                                {{__('site.dashboard')}}
                                @else 
                                Dashobard
                                @endif

                            </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('/admin/admins')}}"
                                aria-expanded="false">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <span class="hide-menu">
                                @if(url()->current()==="http://localhost:8000/admin/services"|| url()->current()==="http://localhost:8000/admin/offers"||url()->current()==="http://localhost:8000/admin/departments")
                                {{__('site.admin')}}
                                @else 
                                Admin
                                @endif
                              </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('/admin/permissions')}}"
                                aria-expanded="false">
                                <i class="fa fa-table" aria-hidden="true"></i>
                                <span class="hide-menu">
                                    @if(url()->current()==="http://localhost:8000/admin/services"|| url()->current()==="http://localhost:8000/admin/offers"||url()->current()==="http://localhost:8000/admin/departments")
                                {{__('site.permission')}}
                                @else 
                                Permission&Roles
                                @endif </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/users')}}"
                                aria-expanded="false">
                                <i class="fa fa-font" aria-hidden="true"></i>
                                <span class="hide-menu">
                                     @if(url()->current()==="http://localhost:8000/admin/services"|| url()->current()==="http://localhost:8000/admin/offers"||url()->current()==="http://localhost:8000/admin/departments")
                                {{__('site.users')}}
                                @else 
                                Users
                                @endif
                                </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/services')}}"
                                aria-expanded="false">
                                <i class="fa fa-globe" aria-hidden="true"></i>
                                <span class="hide-menu">
                                @if(url()->current()==="http://localhost:8000/admin/services"|| url()->current()==="http://localhost:8000/admin/offers"||url()->current()==="http://localhost:8000/admin/departments")
                                {{__('site.services')}}
                                @else 
                                Services
                                @endif</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/departments')}}"
                                aria-expanded="false">
                                <i class="fa fa-columns" aria-hidden="true"></i>
                                <span class="hide-menu">
                                    @if(url()->current()==="http://localhost:8000/admin/services"|| url()->current()==="http://localhost:8000/admin/offers"||url()->current()==="http://localhost:8000/admin/departments")
                                {{__('site.departments')}}
                                @else 
                                Departmrnts
                                @endif
                                </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('admin/offers')}}"
                                aria-expanded="false">
                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                <span class="hide-menu">@if(url()->current()==="http://localhost:8000/admin/services"|| url()->current()==="http://localhost:8000/admin/offers"||url()->current()==="http://localhost:8000/admin/departments")
                                {{__('site.offers')}}
                                @else 
                                Offers
                                @endif           

</span>
                            </a>
                        </li>
                        
                    </ul>

                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>

        

