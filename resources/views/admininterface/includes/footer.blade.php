 
 <footer class="footer text-center"> 2021 © Ample Admin brought to you by <a
                    href="https://www.wrappixel.com/">wrappixel.com</a>
            </footer>
            
        </div>
        
    <script src="{{asset('adminfile/plugins/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('adminfile/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('adminfile/js/app-style-switcher.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset('adminfile/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('adminfile/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('adminfile/js/custom.js')}}"></script>
    <script type="text/javascript">
        $('.add_admin').click(function(event)
         {
             $('#models').modal('show');
        });
         $('.close,.submit').click(function(event) {

               $('#models').modal('hide');
          });
         
         $('.ok,.cancel').click(function(event) 
         {

               $('#model').modal('hide');
        });
         
         $('.btn_edit').click(function(event)
          {
               $('#model').modal('show');
           });
         $('.delete_admin').click(function(event)
         {
             $('#exampleModal').modal('show');
        });
         $('.role').click(function(){
            $("#role").modal('show');

         });
         $('.permission').click(function(){
            $("#permission").modal('show');

         });
          $('.edit_permission').click(function(){
            $("#edit_permission").modal('show');

         });
          $('.edit_Role').click(function(){
            $("#edit_Role").modal('show');

         });
          $('.delete_Per').click(function(){
            $("#delete_Per").modal('show');

         });
          $('.delete_role').click(function(){
            $("#delete_role").modal('show');

         });
          $('#addUser').click(function(){
            $("#exampleModal").modal('show');

         });
          $('.edit_user').click(function(event) {
            var id=$(this).attr('edit_user');

            $.ajax({
                url: '{{route("edit.user")}}',
                type:'GET',
                data: {"_token": "{{ csrf_token() }}",id:id},
            })
            .done(function(data_edit) {
                 $('#editUser').modal('show');

                console.log(data_edit);


            })
            .fail(function(e) {
                console.log(e);
            });
            
            
          });
          // $('.user').submit(function(e) {
          //    e.preventDefault();
          // });
          $('#addUsers').click(function(){
            $("#exampleModalll").modal('show');

         });
          $('.ed_us').click(function(){
            $("#editusers").modal('show');

         });
          $('.delete_user').click(function(event) {
            $("#delete").modal('show');
          });
          $('.editt').click(function(event) {
            $("#editusers").modal('hide');
          });
          $('.add_depart').click(function(event) {
            $("#depart_add").modal('show');
          });
          $('.depart_edit').click(function(event) {
            $("#depart_edit").modal('show');
          });
          
          $('.del_depart').click(function(event) {
            $("#depart_del").modal('show');
          });




         
    </script>
        <script type="text/javascript">
          $(document).ready(function() {
               $(".add_service").click(function(a) {
               a.preventDefault();
               var formdata=new FormData(form);
  
               $.ajax({
                 url:"{{route('service')}}",
                 type: 'POST',
                 data:formdata,
                  processData: false,
                 contentType: false,
               })
               .done(function(data) {
                 console.log(data);
               })
               .fail(function(e) {
                 console.log(e);
               });

            });
               var service_id;
               $('.serve_edit').click(function(t) {
                t.preventDefault();
                service_id=$(this).attr('serv_edit');
                $.ajax({
                  url: '{{route("show.service")}}',
                  type: 'POST',
                  dataType: 'json',
                  data: {"_token": "{{ csrf_token() }}",id: service_id},
                })
                .done(function(c) {
                  $('.edit_s').val(c.id);
                  $('.name').val(c.name);
                  $('.desc').val(c.description);
                  $('.start_e').val(c.date_of_start);
                  $('.end_e').val(c.date_of_end);
                  $('.place_e').val(c.place);
                  $('.depart_e').val(c.department_id);
                  $('.user_e').val(c.user_id);
                })
                .fail(function(r) {
                  console.log(r);
                })
                
               });
               $('.edit_service').click(function(n) {
                n.preventDefault();
                 var form_data=new FormData(form_edit);
                 $.ajax({
                   url: '{{route("update.service")}}',
                   type: 'POST',
                   data: form_data,
                   processData: false,
                 contentType: false,
                 })
                 .done(function(s) {
                   if(s==="please write arabic"){
                    $('.valid').html(s);

                   
                  }
                 })
                 .fail(function(r) {
                   console.log(r);
                 })
                 
                 
               });
               var id_del;
               $('.serve_del').click(function(d) {
                d.preventDefault();
                id_del=$(this).attr('serv_del');
                
               });
               $('.del_service').click(function(event) {
                $.ajax({
                  url: '{{route("del")}}',
                  type: 'POST',
                  data:{"_token": "{{ csrf_token() }}",id: id_del},
                })
                .done(function(ss) {
                  $('.fade bd-example-modal-lggg').modal('hide');
                })
                .fail(function(o) {
                  console.log(o);
                })
                
                
               });




  
        });

        </script>
        <script type="text/javascript">

         $(document).ready(function()
          {
            $(".add_offer").click(function(offer) 
            {
             offer.preventDefault();
             var data_offer=new FormData(offers);
             $.ajax({
               url: '{{route("offer.add")}}',
               type: 'POST',
               data: data_offer,
               processData: false,
                  contentType: false,
            })
            .done(function(success) {
              console.log(success);
              $('.bd-example-modal-llg').modal('hide');

            })
            .fail(function(e){
              $('#hourn').text(e.responseJSON.errors.hour);
              $('#hoursn').text(e.responseJSON.errors.hours);
              $('#user').text(e.responseJSON.errors.user);
              $('#service').text(e.responseJSON.errors.service);
            })
            
          });
            var offer_u;
            $(".offer_edit").click(function(offers) {
              offers.preventDefault();
                offer_u=$(this).attr('edit_offer');
              $.ajax({
                url: '{{route("offer.edit")}}',
                type: 'POST',
                dataType: 'json',
                data: {"_token":"{{csrf_token()}}",id: offer_u},
              })
              .done(function(ss) {
                $('.update_id').val(ss.id);
                $('.hour_e').val(ss.price_of_hour);
                $('.hours_e').val(ss.hours);
                $('.user_e').val(ss.user_id);
                $('.service_e').val(ss.service_id);
              })
              .fail(function(oo) {
                console.log(oo);
              })
              
            });
            $('.offer_update').click(function(offer_update) {
              offer_update.preventDefault();
              var offer_data=new FormData(offers_edit);
              $.ajax({
                url: "{{route('offer.update')}}",
                type: 'POST',
                data: offer_data,
                processData: false,
                 contentType: false,
              })
              .done(function(s) {
                console.log(s);
                 $('#exampleModal').modal('hide');

                
              })
              .fail(function(ro) {
                console.log(ro);
                $('#hour_e').text(ro.responseJSON.errors.hour_e);
              $('#hoursn_e').text(ro.responseJSON.errors.hours_e);
              $('#user_e').text(ro.responseJSON.errors.users_e);
              $('#service_e').text(ro.responseJSON.errors.service_e);
              })
              
              
            });
            var id_offer;
            $('.offer_del').click(function(offer_del) {
              offer_del.preventDefault();
              id_offer=$(this).attr('offer_del');

            });
            $('.offer_delete').click(function(offer_delete) {
              offer_delete.preventDefault();
              $.ajax({
                url: '{{route("delete.offer")}}',
                type: 'POST',
               
                data: {id: id_offer,"_token":"{{csrf_token()}}"},
              })
              .done(function(j) {
                console.log(j);
                $('#exampleModall').modal('hide');


              })
              .fail(function(k) {
                console.log(k);
              })
              
              
            });




            
        });
                




        </script>


    @livewireScripts

</body>

</html>