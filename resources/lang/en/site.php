<?php
return[

'admin'=>'الأدمن',
'dashboard'=>'الرئيسيه',
'add'=>'إضافه',
'service'=>'خدمه',
'description'=>'التفاصيل',
'start date'=>'تاريح البدء',
'finish date'=>'تاريخ الأنتهاء',
'place'=>'المكان',
'department'=>'القسم',
'name'=>'اسم ',
'user'=>'العميل',
'status'=>'الحاله',
'controls'=>'أدوات التحكم',
'close'=>'اغلاق',
'save'=>'حفظ التغيرات',
'edit'=>'تعديل',
'delete'=>'حذف',
'do you want'=>'هل تريد',
'this'=>'هذه',
'permission'=>'صلاحيات',
'users'=>'العملاء',
'services'=>'الخدمات ',
'departments'=>'الأقسام ',
'offers'=>'العروض',
'offer'=>'عرض',
'price'=>'سعر',
'hour'=>'الساعه',
'number'=>'عدد',
'hours'=>'الساعات',
'en'=>"اللغه الانجليزيه ",
'img'=>"صوره",
'enter'=>"ادخال"
];

?>